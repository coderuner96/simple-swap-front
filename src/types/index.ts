export interface ICoinItem {
  symbol: string
  has_extra_id: boolean
  extra_id: string
  name: string
  warnings_from: any[]
  warnings_to: any[]
  validation_address: string
  validation_extra: string
  address_explorer: string
  tx_explorer: string
  confirmations_from: string
  image: string
}
