import React, { useCallback, useEffect, useState } from 'react'

import axios from 'axios'

import { ICoinItem } from '../../types'

import styles from './styles.module.scss'
import SearchBlock from './components/search-block'
import CoinCard from './components/coin-card'

const requestUrl = 'http://localhost:7777/api/v1/all-currencies'

export default function TableCoins () {
  const [staticData, setStaticData] = useState<ICoinItem[]>([])
  const [coinList, setCoinList] = useState<ICoinItem[]>([])
  const [currenciesPage, setCurrenciesPage] = useState(1)
  const [currenciesPerPage] = useState(20)
  const [searchValue, setSearchValue] = useState('')
  const pageNumbers: number[] = []

  const lastCurrenciesIndex = currenciesPage * currenciesPerPage
  const firstCurrenciesIndex = lastCurrenciesIndex - currenciesPerPage

  const currentCountry = coinList.slice(firstCurrenciesIndex, lastCurrenciesIndex)

  for (let i = 1; i <= Math.ceil(coinList.length / currenciesPerPage); i++) {
    pageNumbers.push(i)
  }

  useEffect(() => {
    const fetchData = () => {
      void axios.get(requestUrl, {
        cancelToken: source.token
      })
        .then(({ data }) => {
          setStaticData([...data.currenciesList])
        })
    }

    const CancelToken = axios.CancelToken
    const source = CancelToken.source()

    fetchData()
    const updateInterval = setInterval(() => fetchData(), 60000)

    return () => {
      clearInterval(updateInterval)
      source.cancel()
    }
  }, [])

  useEffect(() => {
    setCoinList(filterHandler(staticData))
  }, [searchValue, staticData])

  const switchPage = (pageNumber: number) => setCurrenciesPage(pageNumber)

  const nextPage = useCallback(() => {
    if (currenciesPage === pageNumbers.length) {
      setCurrenciesPage(pageNumbers[0])
    } else {
      setCurrenciesPage(prevState => prevState + 1)
    }
  }, [])

  const prevPage = useCallback(() => {
    if (currenciesPage === pageNumbers[0]) {
      setCurrenciesPage(pageNumbers[pageNumbers.length - 1])
    } else {
      setCurrenciesPage(prevState => prevState - 1)
    }
  }, [])

  const changeInputValue = (value: string) => {
    setSearchValue(value)
  }

  const clearSearchValue = () => setSearchValue('')

  const trimLowerHandler = (value: string) => {
    return value.trim().toLowerCase()
  }

  const filterHandler = (array: ICoinItem[]) => {
    setCurrenciesPage(1)
    return array.filter(item => {
      return trimLowerHandler(item.name).includes(trimLowerHandler(searchValue)) ||
             trimLowerHandler(item.symbol).includes(trimLowerHandler(searchValue))
    })
  }

  return (
    <main className={styles.table}>
      <SearchBlock
        searchValue={searchValue}
        changeInputValue={changeInputValue}
        clearSearchValue={clearSearchValue}
      />
      <CoinCard
        currentCountry={currentCountry}
        pageNumbers={pageNumbers}
        prevPage={prevPage}
        nextPage={nextPage}
        switchPage={switchPage}
      />
    </main>
  )
}
