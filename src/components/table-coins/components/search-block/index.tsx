import React, { useState } from 'react'

import cn from 'classnames'

import styles from './styles.module.scss'

interface Props {
  searchValue: string
  changeInputValue: (value: string) => void
  clearSearchValue: () => void
}

export default function SearchBlock ({ searchValue, changeInputValue, clearSearchValue }: Props) {
  const [toggleSearch, setToggleSearch] = useState(false)

  const toggleSearchInput = () => {
    setToggleSearch(!toggleSearch)
  }

  return (
    <div
      className={cn(styles.search__block, toggleSearch && styles.open)}
    >
      <input
        type="text"
        className={styles.search__input}
        placeholder='Поиск по списку монет'
        onChange={(event) => changeInputValue(event.target.value)}
        value={searchValue}
      />
      <span
        className={cn(styles.search__clear, toggleSearch && styles.show)}
        onClick={clearSearchValue}
      />
      <button
        className={styles.search__button}
        onClick={toggleSearchInput}
      >
        <span className={styles.search__icon}/>
      </button>
    </div>
  )
}
