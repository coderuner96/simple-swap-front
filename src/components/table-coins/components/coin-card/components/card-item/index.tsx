import React from 'react'

import { ICoinItem } from '../../../../../../types'

import styles from './styles.module.scss'

interface Props {
  data: ICoinItem
}

export default function CardItem ({ data }: Props) {
  const { name, symbol, image } = data

  const baseUrlImage = `https://simpleswap.io${image}`

  return (
    <div className={styles.card__item}>
      <div className={styles.card__item_image}>
        <img
          src={baseUrlImage}
          alt="icon"
        />
      </div>
      <div className={styles.card__item_description}>
        <div className={styles.item__name}>{name}</div>
        <div className={styles.item__symbol}>{symbol}</div>
      </div>
    </div>
  )
}
