import React from 'react'

import styles from './styles.module.scss'
import PaginationButton from './pagination-button'

interface Props {
  pageNumbers: any[]
  switchPage: (number: any) => void
  nextPage: () => void
  prevPage: () => void
}

export default function Pagination ({ pageNumbers, switchPage, nextPage, prevPage }: Props) {
  return (
    <div className={styles.pagination}>
      <PaginationButton func={prevPage} title='Prev'/>
      <ul className={styles.pagination__list}>
        {pageNumbers.map(number => (
          <li
            onClick={() => switchPage(number)}
            className={styles.pagination__list_item}
            key={number}
          >{number}</li>
        ))}
      </ul>
      <PaginationButton title='Next' func={nextPage}/>
    </div>
  )
}
