import React from 'react'

import styles from './styles.module.scss'

interface Props {
  func: () => void
  title: string
}

export default function PaginationButton ({ func, title }: Props) {
  return (
    <button
      className={styles.pagination__button}
      onClick={func}
    >
      <span>
        {title}
      </span>
    </button>
  )
}
