import React from 'react'

import Pagination from './components/pagination'

import styles from './styles.module.scss'
import CardItem from './components/card-item'

interface Props {
  pageNumbers: any[]
  currentCountry: any[]
  switchPage: (number: any) => void
  nextPage: () => void
  prevPage: () => void
}

export default React.memo(function CoinCard ({
  pageNumbers,
  nextPage,
  prevPage,
  switchPage,
  currentCountry
}: Props) {
  return (
    <>
      <div className={styles.card}>
        {currentCountry.map(item => (
            <CardItem key={item.name} data={item}/>
        )
        )}
      </div>
      <Pagination
        pageNumbers={pageNumbers}
        switchPage={switchPage}
        nextPage={nextPage}
        prevPage={prevPage}
      />
    </>

  )
})
