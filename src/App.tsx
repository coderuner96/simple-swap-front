import React from 'react'

import styles from './global-styles/app.module.scss'
import TableCoins from './components/table-coins'

function App () {
  return (
    <div className={styles.app}>
      <TableCoins/>
    </div>
  )
}

export default App
